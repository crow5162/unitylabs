﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonLoader : MonoBehaviour
{
    //Serialization < 직렬화 >
    //Object를 연속된 문자형 데이터나 연속된 Byte형 데이터로 바꾸는것을 말한다.

    //이렇게 직렬화 하는 이유 ?
    //Object는 메모리에 존재하고 추상적인데 비해서 String과 Byte 데이터는 
    //드라이브에 저장도 할 수 있고, 통신선을 통해서 전송할 수 있기 때문이다 !

    //DeSerialization < 역직렬화 >
    //반대로 이미 존재하는 String Data나 Byte Data, Byte File로 부터 Object를 생성하는 행위는 
    //역직렬화 라고 한다. 

    //우리가 유니티에서 GameObject를 추가하거나 수정하는 작업도
    //유니티 에디터를 종료하고 다시 실행시켰을때 남아있을수 있는 이유는 
    //GameObject도 Serialization 되어서 드라이브에 텍스트 파일로 남아있기 때문에
    //유니티를 종료하여 파괴되어도 역직렬화를 거쳐, 다시 불러올 수 있는 것이다.

    //사람으로 치면 냉동인간으로 만들어서 미래에 다시 깨운다고 보면 되겠다 

    //       < 단순 텍스트 파일 >
    //Object -> Bytes
    //       -> XML
    //       -> JSON
    //       -> YAML
}
