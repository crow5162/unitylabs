﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CubeController : MonoBehaviour
{
    //CubeController 로써 필요한 변수와 메서드가 있다..
    public CubeData cubeData; 

    //< 큐브의 데이터를 저장하는 함수 >
    //ContextMenu는 컴포넌트에 뒤따라오는 함수를 실행하는
    //메뉴버튼을 추가해줍니다
    [ContextMenu("To Json Data")]
    void SaveCubeDataToJson()
    {
        //Json문자열로 변형하고 싶은 오브젝트를 넣어주면 
        //Json형태로 Formating된 문자열이 나옵니다
        //< JsonUtility의 ToJson 메서드의 두번째 인자는 PrettyPrint 불린값은 
        //저장할 Json파일을 사람이 읽기 좋은 형태로 변환해서 저장해줍니다 >
        string jsonData = JsonUtility.ToJson(cubeData, true);

        //저장될 파일의 경로는 현재 실행될 유니티의 내부에 저장합니다

        // DataPath는 현재 유니티 에디터를 실행하는 입장에서는 현재 실행중인 유니티 프로젝트의 경로가 될 것입니다
        string path = Path.Combine(Application.dataPath, "CubeData.json");
        File.WriteAllText(path, jsonData);
    }

    //< 큐브의 데이터를 읽어오는 함수 >
    [ContextMenu("From Json Data")]
    void LoadCubeDataFromJson()
    {
        //경로는 같기때문에 남겨줍니다
        string path = Path.Combine(Application.dataPath, "CubeData.json");

        //json데이터로 부터 데이터를 가져와서 덮어씌워야 합니다
        string jsonData = File.ReadAllText(path);

        //Json으로부터 Object를 역직렬화 할 수 이따
        cubeData = JsonUtility.FromJson<CubeData>(jsonData);
    }
}

//생성한 Class가 Serializable 속성을 붙여주지 않았을때 
//유니티 에디터에서 수정이 불가능한 이유?
//Serializable 속성을 붙여주지 않으면 오브젝트의 편집이 불가능하다
//Unity Inepector에서 값을 수정한다는 것은 말하자면 텍스트 파일을 수정하는 것이다
//즉 Object가 파일로써 저장 가능한 형태가 아니라면 편집이 불가능하다
//객체를 실시간으로 메모리를 생성할 수는 있지만...
[System.Serializable]
public class CubeData
{
    public string name;
    public int age;
    public int level;
    public bool isDead;
    public string[] items;


}
