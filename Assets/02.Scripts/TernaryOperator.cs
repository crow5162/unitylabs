﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TernaryOperator : MonoBehaviour
{
    //삼항연산자
    public int intExam;

    private bool isEven;

    private void Start()
    {
        //삼항연산자는 피연산자가 3개인 조건부 연산자를 의미합니다
        //is else 구문을 한줄로 간단하게 표현할 수 있기 때문에 인라인 (inline if)이라고도 합니다
        //if else 구문과 결과는 동일하지만 if else구문은 여러줄로 작성되는 반면, 삼항연산자를 
        //사용하면 한 줄로 간단하게 표현할 수 있습니다

        //Ex.01
        //삼항연산자 !
        //isEven = (intExam % 2 == 0) ? true : false;
        //사망연산자 미사용 !
        //if(intExam % 2 == 0)
        //{
        //    isEven = true;
        //}
        //else
        //{
        //    isEven = false;
        //}

        //코드가 간결해진다 ! Magic !

        //Ex.02
        //삼항연산자 !
        //intExam = isEven ? 10 : 15;
        //삼항연산자 미사용 !
        //if(isEven == true)
        //{
        //    intExam = 10;
        //}
        //else
        //{
        //    intExam = 15;
        //}
        
        //변수 = (조건) ? 참 : 거짓; 
        //조건이 True 라면 변수에 참이 담기고, 
        //조건이 False라면 변수에 거짓이 담긴다.
    }
}
