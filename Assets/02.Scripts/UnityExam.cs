﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityExam : MonoBehaviour
{
    List<int> intList = new List<int>();

    private void Start()
    {
        for(int i =0;i<10;i++)
        {
            int randomIndex = Random.Range(0, 10);
            intList.Add(randomIndex);
        }

        //foreach(object elem in intList)
        //{
        //    Debug.Log("intList Element : " + elem);
        //}

        intList.ForEach((elem) => { Debug.Log(elem); });

        //Debug.Log();

        //RandomManager<int>.AddRandomList(10);

    }
}
