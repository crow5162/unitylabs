﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class practice : MonoBehaviour
{
    // 스피드, 수평축, 수직축, 벡터, 쉬프트 버튼 눌렀을때, 애니메이터

    public float speed = 3.0f;
    private float vAxis;
    private float hAxis;
    private Vector3 moveVec;
    private bool buttonDown;
    private Animator anim;

    // 시작

    private void Start()
    {
        // 애니메이터 선언 애니메이터가 붙어있는것은 anim
        anim = GetComponentInChildren<Animator>();
        
    }

    private void Update()
    {
        vAxis = Input.GetAxis("Vertical");
        hAxis = Input.GetAxis("Horizontal");
        buttonDown = Input.GetButton("Walk");

        moveVec = new Vector3(hAxis, 0, vAxis).normalized;
        transform.Translate(speed * moveVec * Time.deltaTime, Space.World);

        //로테이션값
        transform.LookAt(transform.position + moveVec);

    }


}
