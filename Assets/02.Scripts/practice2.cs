﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class practice2 : MonoBehaviour
{
    // 속도, 수평축, 수직축, 벡터, 애니메이터

    public float speed = 4.0f;
    private float hAxis;
    private float vAxis;
    private Vector3 moveVec;
    private Animator anim;


    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }


    private void Update()
    {
        hAxis = Input.GetAxis("Horizontal");
        vAxis = Input.GetAxis("Vertical");

        //움직인 거리는?
        moveVec = new Vector3(hAxis, 0, vAxis).normalized;

        //포지션
        transform.Translate(speed * moveVec * Time.deltaTime, Space.World);

        //로테이션
        transform.LookAt(transform.position + moveVec);

        //애니메이션 바꿔는거 어디...?
        //한번 다른거 안보고 바꿔봐 ! 할수있자나
        
        if(hAxis !=0 || vAxis != 0)
        {
            anim.SetBool("isWalk",true);
        }
       
        //루프어디갔지??? 애니메이션 루프 애니메이션 자체에 있얼
    }
}

