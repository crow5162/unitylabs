﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimMovement : MonoBehaviour
{
    [Header("Sim Movement")]
    [Range(0.0f, 20.0f)] public float moveSpeed = 4.0f;

    private float hAxis;
    private float vAxis;
    private Vector3 moveVec;
    private bool walkDown;

    private Animator anim;
    //readonly나 const는 변수를 상수로 만드는 예약어인데 처음 Initial할때 정해주면후에 그 값을 변경하는게 안되는거야 보통
    //바뀌면 안되는 값에 주로붙여서 사용해 코딩중에 실수로라도 값이 변경되지않게 하는거야

    private readonly int hashRun = Animator.StringToHash("isRun");
    private readonly int hashWalk = Animator.StringToHash("isWalk");

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();

    }

    private void Update()
    {
        hAxis = Input.GetAxisRaw("Horizontal");
        vAxis = Input.GetAxisRaw("Vertical");
        walkDown = Input.GetButton("Walk");

        moveVec = new Vector3(hAxis, 0, vAxis).normalized;
        transform.Translate(moveVec * moveSpeed * (walkDown ? 0.3f : 1f) * Time.deltaTime, Space.World);

        anim.SetBool(hashRun, moveVec != Vector3.zero);
        anim.SetBool(hashWalk, walkDown);

        //Player Rotation
        // 자신의 위치값 position에서 나아가야 할 방향을 바라본다
        transform.LookAt(transform.position + moveVec);
    }
}