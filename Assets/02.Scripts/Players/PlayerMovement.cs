﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
//public struct WormsAnim
//{
//    public AnimationClip _idle;
//    public AnimationClip _walk;
//    public AnimationClip _win;
//}

public class PlayerMovement : MonoBehaviour
{
    [Header("Character Movement")]
    [Range(0.0f, 20.0f)] public float moveSpeed = 5.0f;

    private float hAxis;
    private float vAxis;
    private Vector3 moveVec;
    private bool walkDown;

    private Animator anim;
    //private Animation _anim;
    //public WormsAnim _clips;

    private readonly int hashRun = Animator.StringToHash("isRun");
    private readonly int hashWalk = Animator.StringToHash("isWalk");

    //private bool isJohyun;
    //private int minWookVal;

    private void Start()
    {
        //자식 객체의 컴포넌트 가져오기 !
        anim = GetComponentInChildren<Animator>();
        //_anim = GetComponent<Animation>();
        //_WalkClip = _anim.GetClip("walk");

        #region 삼항연산자 !

        //if(minWookVal % 2 == 0)
        //{
        //    isJohyun = true;
        //}
        //else
        //{
        //    isJohyun = false;
        //}
        //
        ////변수 = (조건) ? 참 : 거짓 <사망연산자>
        //isJohyun = (minWookVal % 2 == 0) ? true : false;

        //if (isJohyun)
        //{
        //    minWookVal = 5;
        //}
        //else
        //{
        //    minWookVal = 10;
        //}
        //
        //minWookVal = (isJohyun) ? 5 : 10;

        #endregion

        //_anim.clip = _clips._idle;
        //_anim.Play();
    }

    private void Update()
    {
        //GetAxis    :: 방향키에 따라 0.0f ~ 1.0f 까지의 실수를 가진다
        //GetAxisRaw :: 방향키에 따라 0과 1두 정수만을 가진다
        hAxis = Input.GetAxisRaw("Horizontal");
        vAxis = Input.GetAxisRaw("Vertical");
        walkDown = Input.GetButton("Walk");

        //Walk 키 입력에 따라 이동속도를 변화하는 삼항연산자 !
        //Translate에 그대로 옮겨 놓았다 :: 코드 간결
        //moveSpeed = walkDown ? 15.0f : 3.0f;

        //normalized :: 방향값이 1로 보정된 벡터 !
        //new Vector3(x, y, z) Vector3는 x, y, z를 가지고 있다 
        moveVec = new Vector3(hAxis, 0, vAxis).normalized;

       //지렁이용 스크립트
       //if(hAxis != 0 || vAxis != 0)
       // {
       //     _anim.clip = _clips._walk;
       // }

       // else
       // {
       //     _anim.clip = _clips._idle;
       // }

        //Translate를 사용한 캐릭터 이동 
        //transform.position += moveVec * moveSpeed * (walkDown ? 0.3f : 1f) *
        //                      Time.deltaTime;

        transform.Translate(moveVec * moveSpeed * (walkDown ? 0.3f : 1f) *
                            Time.deltaTime, Space.World);

        //Transform을 사용하여 움직이는 이동은 물리 충돌을 무시하는 경우가 발생을 하기때문에
        //RigidBody Component의 Collision Detection의 설정을 Diecret -> Continous 로 변경해줍니다 !
        anim.SetBool(hashRun, moveVec != Vector3.zero);
        anim.SetBool(hashWalk, walkDown);

        //Player Rotation
        //자신의 위치값 position에서 나아가야할 방향을 바라본다
        transform.LookAt(transform.position + moveVec);


        //Worms Animation
       
        //_anim.GetClip("walk");
        //_anim.Play();

    }
}
